module Analysis
using Printf

include("Constants.jl")
using .Constants

export corrchk, corrchk_internal

function corrchk(x::Array{Float64,1},ixd::Int64)
# ! Interacting Electrons, Ceperley et al, p585. MC in ab initio quantum chemstry, Hammond et al, p59, eq(2.27).   
    corrin = Array{Float64,1}(undef,ixd-1)
    kappa = Array{Float64,1}(undef,ixd-1)

    xave = sum(x[1:ixd])/ixd
    xvar = sum(x[1:ixd].^2)/ixd-xave^2 
    corrin .= Constants.ZERO
    kappa .= Constants.ZERO   

    io = open("xcorrelationcheck.txt", "w")
    write(io, "k corr(k) kappa \n")
    for lcorr in 1:ixd-1
        for  icorr in 1:ixd-lcorr
            corrin[lcorr] = corrin[lcorr] + (x[icorr]-xave)*(x[icorr+lcorr]-xave)
        end
        corrin[lcorr] = corrin[lcorr]/(ixd-lcorr)/xvar 
		kappa[lcorr] = Constants.ONE + Constants.TWO*sum(corrin[1:lcorr])
        @printf(io, "%10d  %0.f %0.f \n", lcorr,corrin[lcorr],kappa[lcorr] )
    end
    close(io)
return nothing
end

function corrchk_internal(x::Array{Float64,1},ixd::Int64)
    corrin = Array{Float64,1}(undef,ixd-1)
    kappa = Array{Float64,1}(undef,ixd-1)

    xave=sum(x[1:ixd])/ixd
    xvar=sum(x[1:ixd].^2)/ixd-xave^2 
    corrin .= Constants.ZERO
    kappa .= Constants.ZERO

    for lcorr in 1:ixd-1
        for icorr in 1:ixd-lcorr
            corrin[lcorr]=corrin[lcorr] + (x[icorr]-xave)*(x[icorr+lcorr]-xave)
        end
        corrin[lcorr]=corrin[lcorr]/(ixd-lcorr)/xvar 
		kappa[lcorr]=Constants.ONE + Constants.TWO*sum(corrin[1:lcorr])
    end

    kpa = -Constants.ONE
    for i in 1:ixd-1
        if i > Constants.FIVE*kappa[i]
            kpa = max(Constants.ONE,kappa[i])
            break
        end
    end
    if kpa < Constants.ZERO
        println("it looks like kappa is not found!")
    end   
    return kpa    
end


end
