module Mixture

export musigma, mean_covar_init

mutable struct Mean_covar
    mu::Array{Float64,2}
    sigma::Array{Float64,2}
    w::Float64
end

const musigma = Array{Mean_covar,1}() # const is global

function mean_covar_init(kmix::Int64,dim_p::Int64,weight::Array{Float64,1},sigma::Array{Float64,3},mu::Array{Float64,2})
    @assert length(weight) == kmix
    @assert size(sigma) == (kmix,dim_p,dim_p)
    @assert size(mu) == (kmix,dim_p)
    resize!(musigma, kmix) # musigma = Vector{MeanCovar}(undef, kmix) 
    for k in 1:kmix
        musigma[k] = Mean_covar(zeros(dim_p,1),zeros(dim_p,dim_p),0.0)
        musigma[k].mu[1,1] = mu[k,1]
        musigma[k].mu[2,1] = mu[k,2]
        musigma[k].sigma[1,1] = sigma[k,1,1]
        musigma[k].sigma[2,2] = sigma[k,2,2]
        musigma[k].w = weight[k]
    end
    return nothing
end

end
