module Constants
const ZERO = 0.0 ::Float64
const ONE = 1.0 ::Float64
const TWO = 2.0 ::Float64
const THREE = 3.0 ::Float64
const FOUR = 4.0 ::Float64
const FIVE = 5.0 ::Float64
const SIX = 6.0 ::Float64
const SEVEN = 7.0 ::Float64
const EIGHT = 8.0 ::Float64
const NINE = 9.0 ::Float64
const TEN = 10.0 ::Float64
const TENTH = 0.1 ::Float64
const HALF = 0.5 ::Float64
const THIRD = (1.0/3.0) ::Float64
const SIXTH = (1.0/6.0) ::Float64
const NORMALPDF_FACTOR = (1.0/sqrt(2*π)) ::Float64
const CZERO = (0.0 + 0.0im) ::ComplexF64
const CI = (0.0 + 1.0im) ::ComplexF64
const CONE = (1.0 + 0.0im) ::ComplexF64
end
