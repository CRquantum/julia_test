# main program
# require Julia > 1.6
using Pkg
if !in("OffsetArrays",keys(Pkg.installed()))
    println("OffsetArrays not installed, now installing...")
    Pkg.add("OffsetArrays")
end
using OffsetArrays
using LinearAlgebra
using Printf
import Dates

include("Ran.jl")
using .Ran
include("Constants.jl")
using .Constants
#include("Mixture.jl")
#using .Mixture
include("Step.jl")
using .Step
include("Model.jl")
using .Model

imode = 11 # 1 mean V fixed. 2 means full mixing, both V and k are flexable.

# True values -----------------
mu01 = 0.3
sig01 = 0.06
mu02 = 0.6
sig02 = 0.06
mu0V = 20.0
sig0V = 2.0
w01 = 0.8
sig0 = 0.1
# ----------------------------
irn = 90027 # set  the base random number seed
Ran.setseed(irn)

itermax = 50
nsub = 100
nsub1 = trunc(Int64, nsub*w01)
nsub2 = nsub - nsub1
mi = 5
kmix = 2
dim_p = 2
readYji = true
m = trunc(Int64,10^4)
mgauss = trunc(Int64,10^3)
tmp = Array{Float64, 1}(undef, itermax+1)
LL_iter = OffsetArray(tmp, 0:itermax)

fmt_k = Printf.Format("%20s"*"%12i |"^kmix)
fmt_float = Printf.Format("%20s"*"%12.5g |"^kmix)

fmt_k_iter = Printf.Format("%20s"*"%15i               |"^kmix)
fmt_float_iter = Printf.Format("%20s"*" %12.5g                 |"^kmix)
fmt_float_err_iter = Printf.Format(" %12.5g +- %-12.5g |")

# '%%%%%%%%%%%% Initial Conditions %%%%%%%%%%%%'
mu1 = mu01
sig1 = sig01
mu2 = mu02
sig2 = sig02
muV = mu0V
sigV = sig0V
w1 = w01
w2 = 1 - w1
sig = sig0
# -------------------------------------
mu1 = 2.0
mu2 = mu1
muV = 50.0
w1 = 0.5
sig1 = mu1/Constants.THREE # five means 5 sigma ~ mean. so that  values are with 5 sigma, this is very reasonable assumption.
sig2 = mu2/Constants.THREE
sigV = muV/Constants.THREE
w2 = Constants.ONE - w1
sig = 1.0

weight = Array{Float64, 1}(undef, kmix)
sigma = Array{Float64, 3}(undef, kmix,dim_p,dim_p)
mu = Array{Float64, 2}(undef, kmix,dim_p)
for k in 1:kmix
    mu[k,1] = mu1
    mu[k,2] = muV
    sigma[k,1,1] = mu[k,1]/Constants.THREE
    sigma[k,2,2] = mu[k,2]/Constants.THREE
end

if kmix >= 2
    for k in 1:kmix-1
        weight[k] = Constants.ONE/convert(Float64,kmix)
    end
    weight[kmix] = Constants.ONE - sum(weight[1:kmix-1])
else
    weight .= Constants.ONE
end

mean_covar_init(kmix,dim_p,weight,sigma,mu)

println(Dates.now())
@printf("%20s %i \n", "imode = ", imode)
@printf("%20s %i \n", "nsub = ", nsub)
println("----- initial condition -----")
println("read Y? ", readYji)
println("----- Stage One -----")
#println("Sigma = ", sig)
@printf("%20s %i \n", "Sigma = ", sig)
println("----- Stage Two -----")

#=
for k in 1:kmix
    println("Mixture Label k: ", k)
    println("weight = ", weight[k])
    println("Muk = ", mu[k,1])
    println("Muk sig = ", sigma[k,1,1])
    println("MuV = ", mu[k,2])
    println("MuV sig = ", sigma[k,2,2])
end
=#

println(Printf.format(fmt_k, "k =", collect(1:kmix)...))
println(Printf.format(fmt_float, "weight =", weight...))
println(Printf.format(fmt_float, "Muk =", mu[:,1]...))
println(Printf.format(fmt_float, "Muk sig =", sigma[:,1,1]...))
println(Printf.format(fmt_float, "MuV =", mu[:,2]...))
println(Printf.format(fmt_float, "MuV sig=", sigma[:,2,2]...))

# Generate Observations data.
D = 100.0
qt1 = mu01.+sig01*randn(Float64,nsub1)
qt2 = mu02.+sig02*randn(Float64,nsub2)
qt = [qt1; qt2]
V = mu0V.+sig0V*randn(Float64,nsub)
t = [1.5, 2.0, 3.0, 4.0, 5.5]
e = sig0*randn(Float64,(mi,nsub))

step_init(imode,itermax,m,mgauss,mi,nsub,dim_p,kmix,mu1,sig1,mu2,sig2,muV,sigV,w1,w2,sig,D,t)

if readYji
    read_Yji(mi,nsub)
else
    Y = model1(nsub,mi,D,V,qt,t,e)
    push_Yji(Y)

    #println(Y)

end

println("Number of Metropolis trials in each iteration = ",m)
println("Gaussian sample size in each iteration = ",mgauss)
println("Total iteration = ",itermax)

@time begin
    LL_iter[0] = prep()
    println("Initial LogLike = ",LL_iter[0])
    for iter in 1:itermax
        steps(iter)
    end
end

# data analysis

nest = 7
#valmusigma = Array{Float64,2}(undef,kmix,nest-2)
#errormusigma = Array{Float64,2}(undef,kmix,nest-2)
#kappa =  Array{Float64,2}(undef,kmix,nest-2)
valname = Array{String,1}(undef,nest)

valname[1] = "weight"
valname[2] = "Mu_k"
valname[3] = "MuV"
valname[4] = "Sig_Muk"
valname[5] = "Sig_MuV"
valname[6] = "Sigma"
valname[7] = "Loglike"

istart_begin = Int64(ceil(Constants.TENTH*itermax))
istart_end = Int64(ceil(0.9*itermax))
istart_step = Int64(ceil(Constants.TENTH*itermax))


LL_iter[1:itermax]  = get_musigma_maxLL()
io = open("LL_iter.txt", "w")
for iter in 0:itermax
    #write(io, "$(iter) $( LL_iter[iter] ) \n")
    @printf(io, "%10d  %0.f \n", iter, LL_iter[iter])
end
close(io)
println("iMode = ", imode)
for istart in istart_begin:istart_step:istart_end
    valmusigma,errormusigma,kappa,valextra,valextra_error,valextra_kappa = get_musigma(istart,nest-2)
    println("Start From Iteration $(istart) / $(itermax[]) ( $(((itermax[]-istart)/itermax[])*100.0) % )")

    #=
    println("Sigma = $(valextra[1]) +- $(valextra_error[1])")
    for k in 1:kmix[]
        println("Mixture Label k = $(k)")
        println("Weight = $(valmusigma[k,1]) +- $(errormusigma[k,1])")
        println("Mu_k = $(valmusigma[k,2]) +- $(errormusigma[k,2])")
        println("Sig Mu_k = $(valmusigma[k,4]) +- $(errormusigma[k,4])")
        println("Mu_V = $(valmusigma[k,3]) +- $(errormusigma[k,3])")
        println("Sig Mu_V = $(valmusigma[k,5]) +- $(errormusigma[k,5])")
    end
    println("Log Likelihood = $(valextra[2]) +- $(valextra_error[2])")
    =#

    println(@sprintf("%20s %12.5g +- %-12.5g","Sigma =",valextra[1], valextra_error[1]))
    println(Printf.format(Printf.Format("%20s"*"%15i               |"^kmix[]), "k =", collect(1:kmix[])...))
    println(@sprintf("%20s","Weight ="),join(Printf.format.(Ref(fmt_float_err_iter), valmusigma[:,1], errormusigma[:,1])))
    println(@sprintf("%20s","Mu_k ="),join(Printf.format.(Ref(fmt_float_err_iter), valmusigma[:,2], errormusigma[:,2])))
    println(@sprintf("%20s","Sig Mu_k ="),join(Printf.format.(Ref(fmt_float_err_iter), valmusigma[:,4], errormusigma[:,4])))
    println(@sprintf("%20s","Mu_V ="),join(Printf.format.(Ref(fmt_float_err_iter), valmusigma[:,3], errormusigma[:,3])))
    println(@sprintf("%20s","Sig Mu_V ="),join(Printf.format.(Ref(fmt_float_err_iter), valmusigma[:,5], errormusigma[:,5])))
    println(@sprintf("%20s %12.5g +- %-12.5g","Log Likelihood =",valextra[2], valextra_error[2]))
    #println("Log Likelihood = $(valextra[2]) +- $(valextra_error[2])")
    println("")



end






println("Program end normally.")
