module Step
using LinearAlgebra
using DelimitedFiles
using Printf
include("Ran.jl")
using .Ran
include("Constants.jl")
using .Constants
#include("Mixture.jl")
#using .Mixture
include("Analysis.jl")
using .Analysis

export step_init,prep,steps,read_Yji,push_Yji,get_musigma,get_musigma_maxLL,musigma,mean_covar_init

mutable struct Mean_covar
    mu::Array{Float64,2}
    sigma::Array{Float64,2}
    w::Float64
end

const musigma = Array{Mean_covar,1}() # const is global

function mean_covar_init(kmix::Int64,dim_p::Int64,weight::Array{Float64,1},sigma::Array{Float64,3},mu::Array{Float64,2})
    @assert length(weight) == kmix
    @assert size(sigma) == (kmix,dim_p,dim_p)
    @assert size(mu) == (kmix,dim_p)
    resize!(musigma, kmix) # musigma = Vector{MeanCovar}(undef, kmix) 
    for k in 1:kmix
        musigma[k] = Mean_covar(zeros(dim_p,1),zeros(dim_p,dim_p),0.0)
        musigma[k].mu[1,1] = mu[k,1]
        musigma[k].mu[2,1] = mu[k,2]
        musigma[k].sigma[1,1] = sigma[k,1,1]
        musigma[k].sigma[2,2] = sigma[k,2,2]
        musigma[k].w = weight[k]
    end
    return nothing
end

const imode = Ref{Int64}() # const is global, make it a Ref to lock the type
const itermax = Ref{Int64}()
const msample = Ref{Int64}()
const mgauss = Ref{Int64}()
const mi = Ref{Int64}()
const nsub = Ref{Int64}()
const dim_p = Ref{Int64}()
const kmix = Ref{Int64}()

const sig = Ref{Float64}()
const sig_inv = Ref{Float64}()
const SigV = Ref{Float64}()
const mu1 = Ref{Float64}()
const sig1 = Ref{Float64}()
const mu2 = Ref{Float64}()
const sig2 = Ref{Float64}()
const muV = Ref{Float64}()
const w1 = Ref{Float64}()
const w2 = Ref{Float64}()

const mitot = Ref{Int64}()
const mgauss_tot = Ref{Int64}()
const mgauss_max = Ref{Int64}()
const mgauss_min = Ref{Int64}()

const iter = Ref{Int64}()

const sigma_factor = Ref{Float64}()
const D = Ref{Float64}()
const normpdf_factor_pYq_i = Ref{Float64}()
const log_normpdf_factor_pYq_i = Ref{Float64}()


const h_diag = Ref(Array{Float64,3}(undef,0,0,0))
const ymh = Ref(Array{Float64,3}(undef,0,0,0))
const thetas = Ref(Array{Float64,3}(undef,0,0,0))


const muk_iter = Ref(Array{Float64,2}(undef,0,0))
const sigk_iter = Ref(Array{Float64,2}(undef,0,0))
const muV_iter = Ref(Array{Float64,2}(undef,0,0))
const sigV_iter = Ref(Array{Float64,2}(undef,0,0))
const wk_iter = Ref(Array{Float64,2}(undef,0,0))
const mukerror_iter = Ref(Array{Float64,2}(undef,0,0))
const sigkerror_iter = Ref(Array{Float64,2}(undef,0,0))
const ar_gik_k = Ref(Array{Float64,2}(undef,0,0))
const muVerror_iter = Ref(Array{Float64,2}(undef,0,0))
const sigVerror_iter = Ref(Array{Float64,2}(undef,0,0))
const log_pYq_km = Ref(Array{Float64,2}(undef,0,0))
const nik = Ref(Array{Float64,2}(undef,0,0))
const nik_sig = Ref(Array{Float64,2}(undef,0,0))
const wknik = Ref(Array{Float64,2}(undef,0,0))
const wknik_sig = Ref(Array{Float64,2}(undef,0,0))
const tauik = Ref(Array{Float64,2}(undef,0,0))
const Yji = Ref(Array{Float64,2}(undef,0,0))

const mgauss_ik = Ref(Array{Int64,2}(undef,0,0))
const isub_Metroplis_gik_all = Ref(Array{Int64,2}(undef,0,0))

const LL_iter = Ref(Array{Float64,1}(undef,0))
const sigma_iter = Ref(Array{Float64,1}(undef,0))
const ar_gik = Ref(Array{Float64,1}(undef,0))
const sigmaerror_iter = Ref(Array{Float64,1}(undef,0))
const norm = Ref(Array{Float64,1}(undef,0))
const norm_sig = Ref(Array{Float64,1}(undef,0))
const normerr = Ref(Array{Float64,1}(undef,0))
const log_norm = Ref(Array{Float64,1}(undef,0))
const wnow = Ref(Array{Float64,1}(undef,0))
const log_wnow = Ref(Array{Float64,1}(undef,0))
const t = Ref(Array{Float64,1}(undef,0))

const fmt_k_iter = Ref{Any}()
const fmt_float_iter = Ref{Any}()
const fmt_float_err_iter = Ref{Any}()

function step_init(imodein::Int64,itermaxin::Int64,min::Int64,mgaussin::Int64,miin::Int64,nsubin::Int64,pin::Int64,kmixin::Int64
                ,mu1in::Float64,sig1in::Float64,mu2in::Float64,sig2in::Float64
                ,muVin::Float64,sigVin::Float64 
                ,w1in::Float64,w2in::Float64,sigin::Float64
                ,Din::Float64,tin::Array{Float64,1})
    @assert length(tin) == miin
    imode[]=imodein
    itermax[]=itermaxin
    msample[]=min
    mgauss[]=mgaussin
    mi[]=miin
    nsub[]=nsubin
    dim_p[]=pin
    kmix[]=kmixin
    sig[]=sigin
    sig_inv[]=Constants.ONE/sig[]
    SigV[]=sigVin
    mu1[]=mu1in
    sig1[]=sig1in
    mu2[]=mu2in
    sig2[]=sig2in
    muV[]=muVin
    w1[]=w1in
    w2[]=w2in
    mitot[]=mi[]*nsub[]
    sigma_factor[]=nsub[]/mitot[]
    D[]=Din
    normpdf_factor_pYq_i[] = Constants.NORMALPDF_FACTOR^mi[]
    log_normpdf_factor_pYq_i[] = log(normpdf_factor_pYq_i[])
    mgauss_tot[] = mgauss[]*kmix[]
    mgauss_max[] = ceil(mgauss_tot[]*0.95)-1
	mgauss_min[] = mgauss_tot[] - mgauss_max[]  

    #resize!(isub_Metroplis_gik_all,msample[]*kmix[])
    #reshape(isub_Metroplis_gik_all,(msample[],kmix[]))

    isub_Metroplis_gik_all[] = Array{Int64,2}(undef,msample[],kmix[])
    log_pYq_km[]=Array{Float64,2}(undef,msample[],kmix[])

    LL_iter[]=Array{Float64,1}(undef,itermax[])
    sigma_iter[]=Array{Float64,1}(undef,itermax[])
    ar_gik[]=Array{Float64,1}(undef,itermax[])
    sigmaerror_iter[]=Array{Float64,1}(undef,itermax[])
    norm[]=Array{Float64,1}(undef,nsub[])
    norm_sig[]=Array{Float64,1}(undef,nsub[])
    normerr[]=Array{Float64,1}(undef,nsub[])
    log_norm[]=Array{Float64,1}(undef,nsub[])
    wnow[]=Array{Float64,1}(undef,kmix[])
    log_wnow[]=Array{Float64,1}(undef,kmix[])
    t[]=Array{Float64,1}(undef,mi[])
    
    muk_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    sigk_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    muV_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    sigV_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    wk_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    mukerror_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    sigkerror_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    ar_gik_k[]=Array{Float64,2}(undef,kmix[],itermax[])
    muVerror_iter[]=Array{Float64,2}(undef,kmix[],itermax[])
    sigVerror_iter[]=Array{Float64,2}(undef,kmix[],itermax[])

    nik[]=Array{Float64,2}(undef,nsub[],kmix[])
    nik_sig[]=Array{Float64,2}(undef,nsub[],kmix[])
    wknik[]=Array{Float64,2}(undef,nsub[],kmix[])
    wknik_sig[]=Array{Float64,2}(undef,nsub[],kmix[])
    tauik[]=Array{Float64,2}(undef,nsub[],kmix[])
    Yji[]=Array{Float64,2}(undef,mi[],nsub[])

    mgauss_ik[]=Array{Int64,2}(undef,nsub[],kmix[])

    h_diag[]=Array{Float64,3}(undef,mi[],kmix[],msample[])
    ymh[]=Array{Float64,3}(undef,mi[],kmix[],msample[])
    thetas[]=Array{Float64,3}(undef,dim_p[],msample[],kmix[])
    
    t[]=tin
    for i in 1:kmix[]
        wnow[][i] = musigma[i].w
    end
    mgauss_ik[] .= mgauss[]


    fmt_k_iter[] = Printf.Format("%20s"*"%15i               |"^kmix[])
    fmt_float_iter[] = Printf.Format("%20s"*" %12.5g                 |"^kmix[])
    fmt_float_err_iter[] = Printf.Format(" %12.5g +- %-12.5g |")


    return nothing
end

function prep()
    norm[] .= Constants.ZERO
    for k in 1:kmix[] 
        gauss_thetas!(musigma[k].mu,musigma[k].sigma,@view(thetas[][:,:,k]))
        for i in 1:nsub[]

            #println(thetas[][:,:,k])
            #exit()

            val_pYq,valsig_pYq = MC_gauss_ptheta_w_sig(pYq_i_detail,i,mgauss_ik[][i,k],k)
            
            #println("k= ",k," i= ",i, " val_pYq= ",val_pYq," valsig_pYq =",valsig_pYq)
            
            nik[][i,k] = val_pYq
            wknik[][i,k] = wnow[][k]*val_pYq
            norm[][i] = norm[][i] + wknik[][i,k]
            nik_sig[][i,k] = valsig_pYq
        end
    end
    LL = Constants.ZERO
    for i in 1:nsub[]
        tauik[][i,:] = wknik[][i,:]/norm[][i]   
        log_norm[][i] = log(norm[][i])
        LL = LL + log_norm[][i]
    end
    return LL
end

function steps(it::Int64)
    if imode[] == 11
        step11(it)
    elseif imode[] == 12
        step12(it)
    else 
        println("Illegal imode value: ",imode[])
        exit()
    end
    return nothing
end

function step11(it::Int64)
    Muk = Array{Float64,1}(undef,kmix[])
    error_muk = Array{Float64,1}(undef,kmix[])
    Sigk = Array{Float64,1}(undef,kmix[])
    error_sigk = Array{Float64,1}(undef,kmix[])
    ar_gik_k_more = Array{Float64,1}(undef,kmix[])

    iter[] = it
    for k in 1:kmix[]
        Muk[k],error_muk[k],Sigk[k],error_sigk[k],ar_gik_k_more[k] = Metroplis_gik_k_more_o_log(k,msample[])
    end

    for i in 1:nsub[]
        log_norm[][i] = log(norm[][i])
    end

    if kmix[] >= 2
        for k in 1:kmix[]-1
            wnow[][k] = wnow[][k]/nsub[]*sum(nik[][:,k]./norm[][:])
        end
        wnow[][kmix[]] = Constants.ONE - sum(wnow[][1:kmix[]-1]) 
    else
        wnow[][:] .= Constants.ONE
    end

    log_wnow[][:] = log.(wnow[][:]) 

    MuV,MuV_error,SigV,sigV_error,sigma,sigma_error,ar_gik_all = Metroplis_gik_all_o_log(msample[])    

    # Updating

    sig[] = sigma
    sig_inv[] = Constants.ONE/sig[]

	for k in 1:kmix[]
		musigma[k].mu[1,1] = Muk[k] 
		musigma[k].sigma[1,1] = Sigk[k]
		musigma[k].mu[2,1] = MuV     
		musigma[k].sigma[2,2] = SigV
		musigma[k].w = wnow[][k]
    end

    norm_sig[][:] .= Constants.ZERO
    for k in 1:kmix[]
        for i in 1:nsub[]
			wknik_sig[][i,k] = wnow[][k]*nik_sig[][i,k]
			norm_sig[][i] = norm_sig[][i] + wknik_sig[][i,k]            
        end
    end

    for i=1:nsub[], k=1:kmix[]
        # println("i=",i," k=",k," wknik_sig[][i,k]=",wknik_sig[][i,k]," norm_sig[][i]=",norm_sig[][i])
        mgauss_ik[][i,k] = min(max(Int64(floor(wknik_sig[][i,k]/norm_sig[][i]*mgauss_tot[])),mgauss_min[]),mgauss_max[])  
    end

    LL = prep()

    for k in 1:kmix[]
        wk_iter[][k,it] = wnow[][k]
		muk_iter[][k,it] = Muk[k]
		sigk_iter[][k,it] = Sigk[k] 
		mukerror_iter[][k,it] = error_muk[k]
		sigkerror_iter[][k,it] = error_sigk[k]
		ar_gik_k[][k,it] = ar_gik_k_more[k]
		muV_iter[][k,it] = MuV
		sigV_iter[][k,it] = SigV
		muVerror_iter[][k,it] = MuV_error
		sigVerror_iter[][k,it] = sigV_error        
    end

	sigma_iter[][it] = sig[]  
	sigmaerror_iter[][it] = sigma_error  
	ar_gik[][it] = ar_gik_all
	LL_iter[][it] = LL    

    println(@sprintf("%20s %10i %20s %12.5g","Iteration =",iter[],"  -> Log Likelihood =",LL))
    println(@sprintf("%20s %12.5g +- %12.5g","Sigma =",sigma, sigma_error))
    #=
    for k in 1:kmix[]
        println("k = ", k)
        println("Weight = ", wnow[][k])
        println("Mu_k = ", Muk[k], " +- ", error_muk[k])
        println("Sig Mu_k = ", Sigk[k], " +- ", error_sigk[k])
        println("Mu_V = ", MuV, " +- ", MuV_error)
        println("Sig Mu_V = ", SigV, " +- ", sigV_error )
        println("Mu_k MC acc rate =", ar_gik_k_more[k])
        println("Mu_V MC acc rate =", ar_gik_all)
    end
    println("")
    =#

    println(Printf.format(fmt_k_iter[], "k =", collect(1:kmix[])...))
    println(Printf.format(fmt_float_iter[], "k =", wnow[]...))
    println(@sprintf("%20s","Mu_k ="),join(Printf.format.(Ref(fmt_float_err_iter[]), Muk, error_muk)))
    println(@sprintf("%20s","Sig Mu_k ="),join(Printf.format.(Ref(fmt_float_err_iter[]), Sigk, error_sigk)))
    println(@sprintf("%20s","Mu_V ="),join(Printf.format.(Ref(fmt_float_err_iter[]), MuV, MuV_error)))
    println(@sprintf("%20s","Sig Mu_V ="),join(Printf.format.(Ref(fmt_float_err_iter[]), SigV, sigV_error)))
    println(Printf.format(fmt_float_iter[], "Mu_k MC acc rate =", ar_gik_k_more...))
    println(@sprintf("%20s %12.5g","Mu_V MC acc rate =",ar_gik_all))
    println("")
    return nothing
end

function gauss_thetas!(mu::Array{Float64,2},sigma::Array{Float64,2},thetas ) # array is mutable, but variables are not. 
    @assert size(mu) == (dim_p[],1)
    @assert size(sigma) == (dim_p[],dim_p[])
    m = size(thetas,2)
    for j in 1:dim_p[]
        thetas[j,:] .= mu[j,1] .+ sigma[j,j]*randn(m) 
    end
    return nothing
end

function MC_gauss_ptheta_w_sig(pyq::Function,i::Int64,m::Int64,k::Int64) 
# simply a variable is not mutable, so put valout, valsigout as output. 
    val = Constants.ZERO
    valsig = Constants.ZERO
    for j in 1:m
        valnow = pyq(thetas[][:,j,k],i)    
        val = val + valnow
        valsig = valsig + valnow^2   
    end 
    val = val/m
	valsig = valsig/m
	valsigout = sqrt(abs(valsig-val^2))
	valout = val   
    return valout, valsigout
end

function Metroplis_gik_k_more_o_log(k::Int64,m::Int64)
    m0 = 200
    countall = 0
    count = 0
    ndecor = min(80,Int64(trunc(m/100.0)))
    mctheta = Array{Float64,2}(undef,dim_p[],Int64(trunc(m/ndecor)))

    val = Constants.ZERO
    val2 = Constants.ZERO
    valcount = 0
    isubo = Int64(ceil(rand(1)[1]*nsub[]))

    isub_Metroplis_gik_all[][1,k] = isubo
    fo = pYq_more_o_log(isubo,k,1)

    muo = thetas[][:,1,k] 
    normo = log_norm[][isubo]

    for j in 2:m
        countall = countall + 1 
        rn = rand(1)[1]
        isubn = Int64(ceil(rand(1)[1]*nsub[]))
        isub_Metroplis_gik_all[][j,k]=isubn
        fn = pYq_more_o_log(isubn,k,j)

        if j <= mgauss_ik[][isubn,k]
            normn = log_norm[][isubn] 	
        else
            nik_old = nik[][isubn,k]
			mgauss_old = mgauss_ik[][isubn,k]
			mgauss_new = mgauss_old + 1
			nik_new = ( nik_old*mgauss_old + exp(fn) )/mgauss_new
			nik[][isubn,k] = nik_new
			mgauss_ik[][isubn,k] = mgauss_new  
			norm[][isubn] = norm[][isubn] + wnow[][k]*(nik_new-nik_old)
			normn = log( norm[][isubn] )                 
        end
        
        if log(rn) < fn-fo+normo-normn
            count = count + 1
			fo = fn
			normo = normn   
            muo[:] = thetas[][:,j,k]   
        end

        if (mod(j,ndecor)==0) && (j>m0)
			valnow = muo[1]
			val = val + valnow  
			val2 = val2 + valnow^2       
            valcount = valcount +1
			mctheta[:,valcount] = muo[:]                
        end
    end

    val = val/Float64(valcount)
    val2 = val2/Float64(valcount)
    error = sqrt(abs(val2-val^2)/Float64(valcount))  
    ar = Float64(count/countall)*100.0 

    valsig = Constants.ZERO
    val2sig = Constants.ZERO
    for j in 1:valcount
        valnow = ( mctheta[1,j] - val )^2
        valsig = valsig + valnow
        val2sig = val2sig + valnow^2             
    end
    valsig = valsig/Float64(valcount)
    val2sig = val2sig/Float64(valcount)
    error_sig = sqrt(abs(val2sig-valsig^2)/Float64(valcount))      
    
    valsig = sqrt(valsig)
    error_sig = error_sig/(Constants.TWO*valsig)
    sig_error = error_sig 

    return val,error,valsig,sig_error,ar
end

function Metroplis_gik_all_o_log(m::Int64)
    thetao = Array{Float64,1}(undef,dim_p[])
    hi_diag_o = Array{Float64,1}(undef,mi[])
    yimhi_o = Array{Float64,1}(undef,mi[])

    m0 = 200
    countall = 0
    count = 0 
    ndecor = min(80,Int64(trunc(m/100.0)))
    mctheta = Array{Float64,2}(undef,dim_p[],Int64(trunc(kmix[]*m/ndecor)))
    val = Constants.ZERO
    val2 = Constants.ZERO
    val_sigma = Constants.ZERO
    val2_sigma = Constants.ZERO
    valcount = 0

    for k in 1:kmix[]
        k_kmixo = Int64(ceil(rand(1)[1]*kmix[]))
        isubo = isub_Metroplis_gik_all[][1,k_kmixo]
        fo = log_pYq_km[][1,k_kmixo]
    
        thetao[:] = thetas[][:,1,k_kmixo] 
        normo = log_norm[][isubo]
        hi_diag_o[:] = h_diag[][:,k_kmixo,1]
        yimhi_o[:] = ymh[][:,k_kmixo,1]	
    
        for j in 2:m
            countall = countall + 1   
            k_kmixn = Int64(ceil(rand(1)[1]*kmix[]))
            isubn = isub_Metroplis_gik_all[][j,k_kmixn]
            fn = log_pYq_km[][j,k_kmixn]
            normn = log_norm[][isubn]
            rn = rand(1)[1]   
            if log(rn) < (fn-fo+normo-normn+log_wnow[][k_kmixn]-log_wnow[][k_kmixo])
                count = count + 1
                fo = fn
                normo = normn 
                k_kmixo = k_kmixn
                thetao[:]= thetas[][:,j,k_kmixn]	    
                hi_diag_o[:] = h_diag[][:,k_kmixo,j]	
                yimhi_o[:] = ymh[][:,k_kmixo,j]
            end
            if ( mod(j,ndecor) == 0  ) && ( j > m0)
                valnow = thetao[2]
                val = val + valnow  
				val2 = val2 + valnow^2
				valnow_sigma = dot((yimhi_o[:].*(Constants.ONE./(hi_diag_o[:].^2))),yimhi_o[:])
				val_sigma = val_sigma + valnow_sigma  
				val2_sigma = val2_sigma + valnow_sigma^2								
				valcount = valcount + 1
				mctheta[:,valcount] = thetao[:]          
            end
        end
    end

	val = val/Float64(valcount)
	val2 = val2/Float64(valcount)
	MuV_error = sqrt(abs(val2-val^2)/Float64(valcount))  
	ar = Float64(count)/Float64(countall)*100.0    

	valsig = Constants.ZERO
	val2sig = Constants.ZERO

    for j in 1:valcount
        valnow = ( mctheta[2,j] - val )^2
		valsig = valsig + valnow
		val2sig = val2sig + valnow^2        
    end

	valsig = valsig/Float64(valcount)
	val2sig = val2sig/Float64(valcount)
	errorsig = sqrt(abs((val2sig-valsig^2)/Float64(valcount)))      	
	valsig = sqrt(valsig)
	errorsig = errorsig/(Constants.TWO*valsig)
	sigV_error = errorsig

	val_sigma = val_sigma/Float64(valcount)
	val2_sigma = val2_sigma/Float64(valcount)
	error_sigma = sqrt(abs((val2_sigma-val_sigma^2)/Float64(valcount)))  	
	val_sigma = val_sigma/Float64(mi[])
	error_sigma = error_sigma/Float64(mi[])	
	sigma = sqrt(val_sigma)
	error_sigma = error_sigma/(Constants.TWO*sigma)
	sigma_error = error_sigma

    return val,MuV_error,valsig,sigV_error,sigma,sigma_error,ar
end


function pYq_i_detail(theta::Array{Float64,1},i::Int64)
    @assert length(theta) == dim_p[]
    xk = theta[1]
    fact_mean = theta[2]/D[]
    log_pYq_i = Constants.ZERO
    product_sigma_inv = normpdf_factor_pYq_i[] 
    for j in 1:mi[]
        sigma_inv = sig_inv[]*fact_mean*exp(xk*t[][j])
        #println("j= ",j, "t= ",t[])
        log_pYq_i = log_pYq_i - (Yji[][j,i]*sigma_inv-sig_inv[])^2 
        product_sigma_inv = product_sigma_inv*sigma_inv

        #println("j= ",j,"sigma_inv= ",sigma_inv, " log_pYq_i= ",log_pYq_i," product_sigma_inv =",product_sigma_inv)

    end
    return abs(product_sigma_inv)*exp(Constants.HALF*log_pYq_i)	
end

function pYq_more_o_log(i::Int64,k::Int64,l::Int64)
    fact_mean = D[]/thetas[][2,l,k]
    xk = thetas[][1,l,k]
    log_pYq_i = Constants.ZERO
    product_sigma_inv = normpdf_factor_pYq_i[] 
    for j in 1:mi[]
        h_diag[][j,k,l] = fact_mean*exp(-xk*t[][j])
        sigma_inv = abs(sig_inv[]/h_diag[][j,k,l])
        ymh[][j,k,l] = Yji[][j,i] - h_diag[][j,k,l]	
        log_pYq_i = log_pYq_i - (ymh[][j,k,l]*sigma_inv)^2	
        product_sigma_inv = product_sigma_inv*sigma_inv	  
    end
    log_pYq = log(product_sigma_inv) + Constants.HALF*log_pYq_i
    log_pYq_km[][l,k] = log_pYq
    return log_pYq
end

function get_musigma_maxLL()
    LL_iter_max , it = findmax(LL_iter[])
    println("Max Log Likelihood $(LL_iter_max) was found in iteration $(it).")
return LL_iter[][1:itermax[]]
end

function get_musigma(istart::Int64,nestin::Int64)

    valout = Array{Float64,3}(undef,kmix[],nestin,itermax[])
    val,val2,error,kappa = ntuple(i -> Array{Float64,2}(undef,kmix[],nestin),4)
    valextra,val2extra,valextra_error,valextra_kappa = ntuple(i -> Array{Float64,1}(undef,2),4)

    valout .= Constants.ZERO

    for k in 1:kmix[]
        valout[k,1,:] = wk_iter[][k,:] # wnow(1) 	
		valout[k,2,:] = muk_iter[][k,:] # Muk(1)    
		valout[k,3,:] = muV_iter[][k,:] # MuV
		valout[k,4,:] = sigk_iter[][k,:] # Sigk(1)     
		valout[k,5,:] = sigV_iter[][k,:] # SigV           
    end

	val .= Constants.ZERO
	val2 .= Constants.ZERO
	valextra .= Constants.ZERO
	val2extra .= Constants.ZERO

    for i in istart:itermax[] 
        for k in 1:kmix[]
            val[k,:] = val[k,:] + valout[k,:,i]
			val2[k,:] = val2[k,:] + valout[k,:,i].^2        
        end
        valextra[1] = valextra[1] + sigma_iter[][i] 
		val2extra[1] = val2extra[1] + sigma_iter[][i]^2   
		valextra[2] = valextra[2] + LL_iter[][i]
		val2extra[2] = val2extra[2] + LL_iter[][i]^2    
    end

	j = itermax[]-istart+1
	rj = Float64(j)
	val[:,:] = val[:,:]/rj
	val2[:,:] = val2[:,:]/rj
	valextra[:] = valextra[:]/rj
	val2extra[:] = val2extra[:]/rj    

    for i in 1:nestin
        for k in 1:kmix[]
            kappa[k,i] = corrchk_internal(valout[k,i,istart:itermax[]],j) 
            if kappa[k,i] < Constants.ZERO
                kappa[k,i] = Constants.FOUR
                println("kappa not found, use 4.0 instead ",i)    
            end
            error[k,i] = sqrt(abs(kappa[k,i]*(val2[k,i]-val[k,i]^2)/rj)) 
        end
    end

    valextra_kappa[1] = corrchk_internal(sigma_iter[][istart:itermax[]],j) 
    if valextra_kappa[1] < Constants.ZERO
        valextra_kappa[1] = Constants.FOUR
        println("Sigma kappa not found, use 4.0 instead")
    end

    valextra_kappa[2] = corrchk_internal(LL_iter[][istart:itermax[]],j) 
    if valextra_kappa[2] < Constants.ZERO
        valextra_kappa[2] = Constants.FOUR
        println("LL kappa not found, use 4.0 instead")
    end

    valextra_error[:] = sqrt.(abs.(valextra_kappa[:].*(val2extra[:]-valextra[:].^2)/rj)) 

    return val,error,kappa,valextra,valextra_error,valextra_kappa
end

function read_Yji(miin::Int64,nsubin::Int64)
    tmp = readdlm("Yji.txt")
    Yji[] = tmp'   
    if ( size(Yji[])[1] != miin ) || ( size(Yji[])[2] != nsubin )
        println("The dimension of Yji does not seem to be correct, please check.")
        exit()
    end
return nothing 
end

function push_Yji(Y::Array{Float64,2})
    @assert size(Y) == (mi[],nsub[])
    Yji[] = Y
return nothing
end







end
