module Model

include("Constants.jl")
using .Constants
export model1
function model1(nsubin::Int64,miin::Int64,Din::Float64,Vin::Array{Float64,1}
,kin::Array{Float64,1},tin::Array{Float64,1},ein::Array{Float64,2})
    @assert length(Vin) == nsubin
    @assert length(kin) == nsubin
    @assert length(tin) == miin
    @assert size(ein) == (miin,nsubin)    
    model1 = Array{Float64,2}(undef,miin,nsubin)
    for i in 1:nsubin
        model1[:,i] = Din/Vin[i]*exp.(-tin[:]*kin[i]).*(Constants.ONE .+ ein[:,i])
    end
return model1
end


end
